package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.sheet.modele.character.Character;
import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;

public class SheetPanel extends JPanel {
    public static final int DEFAULT_WIDTH = 500;
    public static final int DEFAULT_HEIGHT = 600;
    public static final Color DEFAULT_BACKGROUND_COLOR = Color.CYAN;

    private Character character;

    private AttributesPanel attributesPanel;
    private StatePanel statePanel;
    private InventoryPanel inventoryPanel;

    public SheetPanel(int width, int height, Color background, @NotNull Character character) throws CharacterNotDefined {
        super();
        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(background);
        this.character = character;
        this.initComponents();
    }

    public SheetPanel(Character character) throws CharacterNotDefined {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_BACKGROUND_COLOR, character);
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public void initComponents() throws CharacterNotDefined {
        this.attributesPanel = new AttributesPanel(this);
        this.statePanel = new StatePanel(SubPanel.DEFAULT_WIDTH, 80, Color.WHITE, this);
        this.inventoryPanel = new InventoryPanel(SubPanel.DEFAULT_WIDTH, 280, Color.WHITE, this);
        this.add(new HeaderPanel(this), BorderLayout.NORTH);
        this.add(this.statePanel, BorderLayout.CENTER);
        this.add(this.attributesPanel, BorderLayout.CENTER);
        this.add(this.inventoryPanel, BorderLayout.SOUTH);
    }

    public void updateAll() {
        this.attributesPanel.updateValues();
        this.statePanel.updateBars();
        this.validate();
        this.repaint();
    }
}
