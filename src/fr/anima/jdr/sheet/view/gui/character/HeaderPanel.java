package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

public class HeaderPanel extends SubPanel {

    public HeaderPanel(int width, int height, Color background, SheetPanel master) throws CharacterNotDefined {
        super(width, height, background, master);
    }

    public HeaderPanel(SheetPanel master) throws CharacterNotDefined {
        super(master);
    }

    @Override
    public void initComponents() throws CharacterNotDefined {
        this.setLayout(new GridLayout(3, 3));

        if (this.getSheet().getCharacter() == null)
            throw new CharacterNotDefined();

        JLabel name = new JLabel("Prénom : " + this.getSheet().getCharacter().getName());
        JLabel surname = new JLabel("Nom : " + this.getSheet().getCharacter().getSurname());
        JLabel nickname = new JLabel("Surnom : " + this.getSheet().getCharacter().getNickname());
        JLabel age = new JLabel("Age : " + this.getSheet().getCharacter().getAge());
        JLabel race = new JLabel("Race : " + this.getSheet().getCharacter().getRace());

        this.add(name);
        this.add(surname);
        this.add(nickname);
        this.add(age);
        this.add(race);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}
