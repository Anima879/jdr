package fr.anima.jdr.sheet.modele.character;

public class Human extends Character {

    private static final String RACE_NAME = "Humain";

    public Human(String name, String surname, String nickname, int age) {
        super(name, surname, nickname, age);
    }

    public Human(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Human(String name, String surname) {
        super(name, surname);
    }

    public Human() {
        super();
    }

    @Override
    public String getRace() {
        return RACE_NAME;
    }
}
