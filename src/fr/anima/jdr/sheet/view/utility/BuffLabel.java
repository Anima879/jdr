package fr.anima.jdr.sheet.view.utility;

import fr.anima.jdr.sheet.modele.utility.Buff;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;

public class BuffLabel extends JLabel {
    private Buff buff;

    public BuffLabel(@NotNull Buff buff) {
        this.buff = buff;
        this.setText((buff.getValue() < 0) ? Integer.toString(buff.getValue()) : "+" + buff.getValue());
        this.setForeground((buff.getValue() < 0) ? Color.RED : Color.GREEN);
        this.setToolTipText(buff.getName());
    }

    public Buff getBuff() {
        return buff;
    }

    public void setBuff(@NotNull Buff buff) {
        this.buff = buff;
        this.setText(Integer.toString(buff.getValue()));
    }
}
