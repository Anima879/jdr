package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.sheet.modele.character.Character;
import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;

import javax.swing.*;
import java.awt.*;

class StatePanel extends SubPanel {

    private JPanel concentrationPanel;
    private JPanel adrenalinPanel;
    private JPanel healthPanel;
    private JPanel manaPanel;
    private JProgressBar healthBar;
    private JProgressBar manaBar;
    private JProgressBar concentrationBar;
    private JProgressBar adrenalinBar;

    StatePanel(int width, int height, Color background, SheetPanel master) throws CharacterNotDefined {
        super(width, height, background, master);
    }

    StatePanel(SheetPanel master) throws CharacterNotDefined {
        super(master);
    }

    void updateBars() {
        int healthValue = this.getSheet().getCharacter().getHealth();
        healthBar.setValue(healthValue);
        healthBar.setString(Integer.toString(healthValue));

        int manaValue = this.getSheet().getCharacter().getMana();
        manaBar.setValue(manaValue);
        manaBar.setString(Integer.toString(manaValue));

        int concentrationValue = this.getSheet().getCharacter().getConcentration();
        concentrationBar.setValue(concentrationValue);
        concentrationBar.setString(Integer.toString(concentrationValue));

        int adrenalinValue = this.getSheet().getCharacter().getAdrenalin();
        adrenalinBar.setValue(adrenalinValue);
        adrenalinBar.setString(Integer.toString(adrenalinValue));

        this.getSheet().repaint();
    }

    @Override
    void initComponents() throws CharacterNotDefined {
        this.setLayout(new GridLayout(2, 2));

        this.healthPanel = new JPanel();
        JLabel healthLabel = new JLabel("Santé");
        this.healthBar = new JProgressBar(0, 100);
        int healthValue = this.getSheet().getCharacter().getHealth();
        healthBar.setValue(healthValue);
        healthBar.setString(Integer.toString(healthValue));
        healthBar.setStringPainted(true);
        healthPanel.add(healthLabel);
        healthPanel.add(healthBar);

        this.manaPanel = new JPanel();
        JLabel manaLAbel = new JLabel("Mana");
        this.manaBar = new JProgressBar(0, 100);
        int manaValue = this.getSheet().getCharacter().getMana();
        manaBar.setValue(manaValue);
        manaBar.setString(Integer.toString(manaValue));
        manaBar.setStringPainted(true);
        manaPanel.add(manaLAbel);
        manaPanel.add(manaBar);

        this.concentrationPanel = new JPanel();
        JLabel concentration = new JLabel("Concentration");
        this.concentrationBar = new JProgressBar(0, 100);
        int concentrationValue = this.getSheet().getCharacter().getConcentration();
        concentrationBar.setValue(concentrationValue);
        concentrationBar.setString(Integer.toString(concentrationValue));
        concentrationBar.setStringPainted(true);
        concentrationPanel.add(concentration);
        concentrationPanel.add(concentrationBar);

        this.adrenalinPanel = new JPanel();
        JLabel adrenalin = new JLabel("Adrénaline");
        this.adrenalinBar = new JProgressBar(0, 100);
        int adrenalinValue = this.getSheet().getCharacter().getAdrenalin();
        adrenalinBar.setValue(adrenalinValue);
        adrenalinBar.setString(Integer.toString(adrenalinValue));
        adrenalinBar.setStringPainted(true);
        adrenalinPanel.add(adrenalin);
        adrenalinPanel.add(adrenalinBar);

        this.add(this.healthPanel);
        this.add(this.manaPanel);
        this.add(this.concentrationPanel);
        this.add(this.adrenalinPanel);
    }
}
