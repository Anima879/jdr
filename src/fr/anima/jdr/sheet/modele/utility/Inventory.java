package fr.anima.jdr.sheet.modele.utility;

import fr.anima.jdr.equipment.exceptions.FullInventoryException;
import fr.anima.jdr.equipment.exceptions.ItemNotInInventoryException;
import fr.anima.jdr.equipment.model.Item;
import fr.anima.jdr.sheet.modele.character.Character;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    private int size;
    private Character owner;
    private List<Item> inventory;

    public Inventory(int size, Character owner) {
        this.size = size;
        this.owner = owner;
        this.inventory = new ArrayList<>();
    }

    public void add(Item item) throws FullInventoryException {
        if (item == null)
            return;
        if (this.getCurrentSize() + item.getInventorySize() > this.size)
            throw new FullInventoryException();

        this.inventory.add(item);
    }

    public Item remove(Item item) throws ItemNotInInventoryException {
        if (inventory.remove(item)) {
            return item;
        } else {
            throw new ItemNotInInventoryException();
        }
    }

    public int getCurrentSize() {
        int sum = 0;
        for (Item item : inventory)
            sum += item.getInventorySize();

        return sum;
    }

    public Inventory() {
        this(0, null);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Character getOwner() {
        return owner;
    }

    public void setOwner(Character owner) {
        this.owner = owner;
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public void setInventory(List<Item> inventory) {
        this.inventory = inventory;
    }
}
