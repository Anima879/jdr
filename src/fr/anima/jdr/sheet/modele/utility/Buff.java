package fr.anima.jdr.sheet.modele.utility;

import fr.anima.jdr.sheet.modele.character.Character;

import java.util.Objects;

public class Buff {
    private String name;
    private Character.ATTRIBUTES attribute;
    private int value;

    public Buff(String name, Character.ATTRIBUTES attribute, int value) {
        this.name = name;
        this.attribute = attribute;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character.ATTRIBUTES getAttribute() {
        return attribute;
    }

    public void setAttribute(Character.ATTRIBUTES attribute) {
        this.attribute = attribute;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.attribute + " " + ((this.value < 0) ? "":"+") + this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buff)) return false;
        Buff buff = (Buff) o;
        return getValue() == buff.getValue() &&
                getName().equals(buff.getName()) &&
                getAttribute() == buff.getAttribute();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAttribute(), getValue());
    }
}
