package fr.anima.jdr.equipment.controler.actions.character;

import fr.anima.jdr.sheet.modele.character.Character;
import fr.anima.jdr.sheet.view.gui.character.SheetPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ModifyAttributeAction extends AbstractAction {

    private final SheetPanel panel;
    private final int modifier;
    private final Character.ATTRIBUTES att;

    public ModifyAttributeAction(SheetPanel panel, int modifier, Character.ATTRIBUTES att) {
        this.panel = panel;
        this.modifier = modifier;
        this.att = att;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int previousValue = this.panel.getCharacter().getAttribute(att);
        this.panel.getCharacter().setAttribute(att, previousValue + this.modifier);
        System.out.println(att + " set from " + previousValue + " to " + this.panel.getCharacter().getAttribute(att));

        this.panel.updateAll();
    }

    public SheetPanel getPanel() {
        return panel;
    }

    public int getModifier() {
        return modifier;
    }

    public Character.ATTRIBUTES getAtt() {
        return att;
    }
}
