package fr.anima.jdr.sheet.modele.exceptions;

public class BuffAlreadyExistException extends Exception {
    public BuffAlreadyExistException() {
    }

    public BuffAlreadyExistException(String message) {
        super(message);
    }
}
