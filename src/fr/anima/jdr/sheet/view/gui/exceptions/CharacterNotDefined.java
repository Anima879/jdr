package fr.anima.jdr.sheet.view.gui.exceptions;

public class CharacterNotDefined extends Exception {
    public CharacterNotDefined() {
        super();
    }

    public CharacterNotDefined(String message) {
        super(message);
    }
}
