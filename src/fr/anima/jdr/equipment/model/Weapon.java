package fr.anima.jdr.equipment.model;

import fr.anima.jdr.sheet.modele.utility.Buff;

public class Weapon extends Item {
    public static final String TYPE = "Arme";

    public Weapon(String name, String effets, Buff bonus, int size){
        this.name = name;
        this.effets = effets;
        this.bonus = bonus;
        this.inventorySize = size;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
