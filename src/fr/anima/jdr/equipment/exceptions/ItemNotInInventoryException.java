package fr.anima.jdr.equipment.exceptions;

public class ItemNotInInventoryException extends Exception {
    public ItemNotInInventoryException() {
    }

    public ItemNotInInventoryException(String message) {
        super(message);
    }
}
