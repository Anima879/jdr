package fr.anima.jdr.sheet.view.utility;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonEditor extends DefaultCellEditor {

    protected JButton button;
    private boolean   isPushed;

    //Constructeur avec une CheckBox
    public ButtonEditor(JCheckBox checkBox) {
        //Par défaut, ce type d'objet travaille avec un JCheckBox
        super(checkBox);
        //On crée à nouveau un bouton
        button = new JButton();
        button.setOpaque(true);
        //On lui attribue un listener
        button.addActionListener(new ButtonListener());
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        //On réaffecte le libellé au bouton
        button.setText("Editor");
        //On renvoie le bouton
        return button;
    }

    //Notre listener pour le bouton
    static class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            //On affiche un message, mais vous pourriez effectuer les traitements que vous voulez
            System.out.println("coucou du bouton : " + ((JButton)event.getSource()).getText());
        }
    }
}
