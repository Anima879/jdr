package fr.anima.jdr.sheet.view.utility;

import fr.anima.jdr.equipment.model.Item;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class InventoryView extends AbstractTableModel {
    private static final String[] HEADER = {"Nom", "Taille", "Type", "Effets", "Bonus", "Modify"};
    private final List<Item> items;

    public InventoryView(List<Item> items) {
        super();
        this.items = items;
    }

    @Override
    public int getRowCount() {
        return items.size();
    }

    @Override
    public int getColumnCount() {
        return HEADER.length;
    }

    @Override
    public String getColumnName(int column) {
        return HEADER[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 5)
            return JButton.class;
        return Object.class;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return items.get(rowIndex).getName();
            case 1:
                return items.get(rowIndex).getInventorySize();
            case 2:
                return items.get(rowIndex).getType();
            case 3:
                return items.get(rowIndex).getEffets();
            case 4:
                if (items.get(rowIndex).getBonus() != null)
                    return items.get(rowIndex).getBonus().toString();
                return "None";
            case 5:
                return "Test";
            default:
                throw new IllegalStateException("Unexpected value: " + columnIndex);
        }
    }

    public void addItem(Item item) {
        items.add(item);
        fireTableRowsInserted(items.size() - 1, items.size() - 1);
    }

    public void removeItem(int rowIndex) {
        items.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
}
