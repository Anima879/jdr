package fr.anima.jdr.equipment.model;

import fr.anima.jdr.sheet.modele.utility.Buff;

import java.util.Objects;

public abstract class Item {
    protected int inventorySize;
    protected String name = "Test";
    protected String effets;
    protected Buff bonus;

    public int getInventorySize() {
        return this.inventorySize;
    }

    public void setInventorySize(int size) {
        this.inventorySize = size;
    }

    public abstract String getType();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffets() {
        return effets;
    }

    public void setEffets(String effets) {
        this.effets = effets;
    }

    public Buff getBonus() {
        return bonus;
    }

    public void setBonus(Buff bonus) {
        this.bonus = bonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return getInventorySize() == item.getInventorySize();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInventorySize());
    }
}
