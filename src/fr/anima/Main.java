package fr.anima;

import fr.anima.jdr.equipment.exceptions.FullInventoryException;
import fr.anima.jdr.equipment.model.Armor;
import fr.anima.jdr.equipment.model.Weapon;
import fr.anima.jdr.sheet.modele.character.Character;
import fr.anima.jdr.sheet.modele.exceptions.BuffAlreadyExistException;
import fr.anima.jdr.sheet.modele.utility.Buff;
import fr.anima.jdr.sheet.view.gui.character.SheetPanel;
import fr.anima.jdr.sheet.modele.character.Human;
import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;

import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) {
        Human test = new Human("Bob", "Bib", 24);
        Weapon arme = new Weapon("Epée du tourment", "Dégât 1D+2", null, 12);
        Armor armure = new Armor("Casque en fer", Character.BODY_PART.HEAD, 13);
        test.getInventory().setSize(1000);

        try {
            test.addInventory(arme);
            test.addInventory(armure);
        } catch (FullInventoryException e) {
            e.printStackTrace();
        }

        try {
            test.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Amulette de force", 2);
            test.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Force du chaos", -1);
        } catch (BuffAlreadyExistException e) {
            e.printStackTrace();
        }

        JFrame frame = new JFrame();
        SheetPanel bob = null;
        try {
            bob = new SheetPanel(test);
        } catch (CharacterNotDefined characterNotDefined) {
            characterNotDefined.printStackTrace();
        }
        frame.add(bob);
        frame.setTitle("Fiche de personnage");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
