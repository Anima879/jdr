package fr.anima.jdr.sheet.modele.character;

import fr.anima.jdr.sheet.modele.exceptions.BuffAlreadyExistException;
import fr.anima.jdr.sheet.modele.exceptions.BuffDoesNotExistException;
import fr.anima.jdr.sheet.modele.utility.Buff;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CharacterTest {

    private Character character;

    @Before
    public void setUp() {
        this.character = new Human();
    }

    @Test
    public void addAttributeBuff() {
        try {
            this.character.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1", 2);
            this.character.addAttributeBuff(Character.ATTRIBUTES.INTELLIGENCE, "Test 2", -2);
        } catch (BuffAlreadyExistException e) {
            e.printStackTrace();
        }

        Buff buff1 = new Buff("Test 1", Character.ATTRIBUTES.FORCE, 2);
        Buff buff2 = new Buff("Test 2", Character.ATTRIBUTES.INTELLIGENCE, -2);

        List<Buff> buffList = this.character.getAttributeBuff(Character.ATTRIBUTES.FORCE);
        assertEquals(buff1, buffList.get(0));
        buffList = this.character.getAttributeBuff(Character.ATTRIBUTES.INTELLIGENCE);
        assertEquals(buff2, buffList.get(0));
    }

    @Test
    public void addAttributeBuffException() {
        try {
            this.character.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1", 2);
            this.character.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1", 2);
            fail();
        } catch (BuffAlreadyExistException e) {
            assertTrue(true);
        }
    }

    @Test
    public void removeAttributeBuff() {
        try {
            this.character.addAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1", 2);
        } catch (BuffAlreadyExistException e) {
            e.printStackTrace();
        }

        try {
            this.character.removeAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1");
        } catch (BuffDoesNotExistException e) {
            e.printStackTrace();
        }
        assertTrue(this.character.getAttributeBuff(Character.ATTRIBUTES.FORCE).isEmpty());
    }

    @Test
    public void removeAttributeBuffException() {
        try {
            this.character.removeAttributeBuff(Character.ATTRIBUTES.FORCE, "Test 1");
            fail();
        } catch (BuffDoesNotExistException e) {
            assertTrue(true);
        }
    }

    @Test
    public void getAttribute() {
        for (Character.ATTRIBUTES att : Character.ATTRIBUTES.values())
            assertEquals(Character.MAX_ATTRIBUTES_VALUE / 2, this.character.getAttribute(att));
    }

    @Test
    public void setAttribute() {
        for (Character.ATTRIBUTES att : Character.ATTRIBUTES.values()) {
            this.character.setAttribute(att, 12);
            assertEquals(12, this.character.getAttribute(att));
        }
    }
}