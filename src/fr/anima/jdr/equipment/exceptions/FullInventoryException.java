package fr.anima.jdr.equipment.exceptions;

public class FullInventoryException extends Exception {
    public FullInventoryException() {

    }

    public FullInventoryException(String s) {
        super(s);
    }
}
