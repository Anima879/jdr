package fr.anima.jdr.equipment.model;

import fr.anima.jdr.sheet.modele.character.Character;

public class Armor extends Item {
    public static final String TYPE = "Armure";

    private String name;
    private Character.BODY_PART bodyPart;
    private int inventorySize;

    public Armor(String name, Character.BODY_PART bodyPart, int inventorySize) {
        this.name = name;
        this.bodyPart = bodyPart;
        this.inventorySize = inventorySize;
    }

    @Override
    public int getInventorySize() {
        return inventorySize;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public void setInventorySize(int inventorySize) {
        this.inventorySize = inventorySize;
    }

    public Character.BODY_PART getBodyPart() {
        return bodyPart;
    }

    public void setBodyPart(Character.BODY_PART bodyPart) {
        this.bodyPart = bodyPart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
