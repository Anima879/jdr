package fr.anima.jdr.sheet.modele.exceptions;

public class BuffDoesNotExistException extends Exception {

    public BuffDoesNotExistException() {
    }

    public BuffDoesNotExistException(String message) {
        super(message);
    }
}
