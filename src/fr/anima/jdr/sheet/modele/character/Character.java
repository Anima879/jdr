package fr.anima.jdr.sheet.modele.character;

import fr.anima.jdr.equipment.exceptions.FullInventoryException;
import fr.anima.jdr.equipment.exceptions.ItemNotInInventoryException;
import fr.anima.jdr.equipment.model.Armor;
import fr.anima.jdr.equipment.model.Item;
import fr.anima.jdr.equipment.model.Weapon;
import fr.anima.jdr.sheet.modele.exceptions.BuffAlreadyExistException;
import fr.anima.jdr.sheet.modele.exceptions.BuffDoesNotExistException;
import fr.anima.jdr.sheet.modele.utility.Buff;
import fr.anima.jdr.sheet.modele.utility.Inventory;

import java.util.ArrayList;
import java.util.List;

public abstract class Character {

    public static final int DEFAULT_AGE = 20;
    public static final int DEFAULT_INVENTORY_SIZE = 20;
    public static final int DEFAULT_MORAL = 50;
    public static final int DEFAULT_HEALTH = 100;
    public static final int DEFAULT_MANA = 50;
    public static final int MAX_ATTRIBUTES_VALUE = 20;
    public static final int MAX_EQUIPED_SLOT = 4;
    public static final int DEFAULT_ADRENALIN = 0;
    public static final int DEFAULT_CONCENTRATION = 50;

    public enum BODY_PART {
        HEAD(0),
        BODY(1),
        ARMS(2),
        LEGS(3),
        FEET(4),
        HANDS(5);

        private final int index;

        BODY_PART(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

    public enum ATTRIBUTES {
        FORCE(0),
        CHARISME(1),
        ADRESSE(2),
        COURAGE(3),
        INTELLIGENCE(4),
        INTUITION(5);

        private final int index;

        ATTRIBUTES(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

    protected int force;
    protected int intelligence;
    protected int charisme;
    protected int adresse;
    protected int courage;
    protected int intuition;
    protected List<Buff> attributesBuff;

    protected String name;
    protected String surname;
    protected String nickname;
    protected int age;

    protected Inventory inventory;
    protected List<Weapon> weaponsSlot = new ArrayList<>();
    protected List<Armor> armor = new ArrayList<>();

    protected int health;
    protected int mana;
    protected int moral;
    protected int adrenalin;
    protected int concentration;

    public Character(String name, String surname, String nickname, int age) {
        this.name = name;
        this.surname = surname;
        this.nickname = nickname;
        this.age = age;
        this.inventory = new Inventory(DEFAULT_INVENTORY_SIZE, this);
        this.moral = DEFAULT_MORAL;
        this.health = DEFAULT_HEALTH;
        this.mana = DEFAULT_MANA;
        this.adrenalin = DEFAULT_ADRENALIN;
        this.concentration = DEFAULT_CONCENTRATION;

        for (int i = 0; i < MAX_EQUIPED_SLOT; i++)
            this.weaponsSlot.add(null);

        for (int i = 0; i < 6; i++)
            this.armor.add(null);

        this.force = MAX_ATTRIBUTES_VALUE / 2;
        this.courage = MAX_ATTRIBUTES_VALUE / 2;
        this.charisme = MAX_ATTRIBUTES_VALUE / 2;
        this.intelligence = MAX_ATTRIBUTES_VALUE / 2;
        this.adresse = MAX_ATTRIBUTES_VALUE / 2;
        this.intuition = MAX_ATTRIBUTES_VALUE / 2;

        this.attributesBuff = new ArrayList<>();
    }

    public Character(String name, String surname, int age) {
        this(name, surname, "None", age);
    }

    public Character(String name, String surname) {
        this(name, surname, "None", DEFAULT_AGE);
    }

    public Character() {
        this("None", "None", "None", DEFAULT_AGE);
    }

    public String toString() {
        return this.name + " " + this.surname + " " + this.nickname + " " + this.health + " " + this.mana;
    }

    public void equipSlot(Weapon weapon, int slot) throws FullInventoryException, ItemNotInInventoryException {
        if (slot > MAX_EQUIPED_SLOT || slot < 0)
            throw new IllegalArgumentException();
        slot--;
        if (weaponsSlot.get(slot) == null) {
            weaponsSlot.set(slot, weapon);
            this.inventory.remove(weapon);
        } else {
            this.inventory.add(weaponsSlot.set(slot, weapon));
        }
    }

    public void desequipSlot(int slot) throws FullInventoryException {
        if (slot > MAX_EQUIPED_SLOT || slot < 0)
            throw new IllegalArgumentException();
        this.inventory.add(weaponsSlot.set(--slot, null));
    }

    public void addInventory(Item item) throws FullInventoryException {
        this.inventory.add(item);
    }

    public void removeInventory(Item item) throws ItemNotInInventoryException {
        this.inventory.remove(item);
    }

    public void equipBody(Armor armor, BODY_PART part) throws FullInventoryException {
        this.inventory.add(this.armor.set(part.getIndex(), armor));
    }

    public void desequipBody(BODY_PART part) throws FullInventoryException {
        if (this.armor.get(part.getIndex()) == null)
            return;

        // Can throw FullInventoryException
        this.inventory.add(this.armor.get(part.getIndex()));
        this.armor.remove(part.getIndex());
    }

    public abstract String getRace();

    public void addAttributeBuff(ATTRIBUTES att, String name, int modifier) throws BuffAlreadyExistException {
        for (Buff buff : this.attributesBuff) {
            if (buff.getName().equals(name) && att == buff.getAttribute())
                throw new BuffAlreadyExistException();
        }

        this.attributesBuff.add(new Buff(name, att, modifier));
    }

    public void removeAttributeBuff(ATTRIBUTES att, String name) throws BuffDoesNotExistException {
        boolean found = false;
        int index = 0;
        for (int i = 0; i < this.attributesBuff.size(); i++) {
            if (this.attributesBuff.get(i).getName().equals(name) && att == this.attributesBuff.get(i).getAttribute()) {
                index = i;
                found = true;
                break;
            }
        }

        if (found) {
            this.attributesBuff.remove(index);
        } else {
            throw new BuffDoesNotExistException();
        }
    }

    // GETTERS AND SETTERS

    public int getAttribute(ATTRIBUTES att) {
        switch (att) {
            case FORCE -> {
                return this.force;
            }
            case CHARISME -> {
                return this.charisme;
            }
            case ADRESSE -> {
                return this.adresse;
            }
            case COURAGE -> {
                return this.courage;
            }
            case INTELLIGENCE -> {
                return this.intelligence;
            }
            case INTUITION -> {
                return this.intuition;
            }
            default -> throw new IllegalStateException("Unexpected value: " + att);
        }
    }

    public void setAttribute(ATTRIBUTES att, int value) {
        value = Math.min(value, MAX_ATTRIBUTES_VALUE);
        value = Math.max(value, 1);

        switch (att) {
            case FORCE -> this.force = value;
            case CHARISME -> this.charisme = value;
            case ADRESSE -> this.adresse = value;
            case COURAGE -> this.courage = value;
            case INTELLIGENCE -> this.intelligence = value;
            case INTUITION -> this.intuition = value;

            default -> throw new IllegalStateException("Unexpected value: " + att);
        }
    }

    public List<Buff> getAttributeBuff(ATTRIBUTES att) {
        List<Buff> buffs = new ArrayList<>();
        for (Buff b : this.attributesBuff) {
            if (b.getAttribute() == att)
                buffs.add(b);
        }

        return buffs;
    }

    public void setAttributesBuff(List<Buff> attributesBuff) {
        this.attributesBuff = attributesBuff;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getMoral() {
        return moral;
    }

    public void setMoral(int moral) {
        this.moral = moral;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Weapon> getWeaponsSlot() {
        return weaponsSlot;
    }

    public void setWeaponsSlot(List<Weapon> weaponsSlot) {
        this.weaponsSlot = weaponsSlot;
    }

    public List<Armor> getArmor() {
        return armor;
    }

    public void setArmor(List<Armor> armor) {
        this.armor = armor;
    }

    public int getAdrenalin() {
        return adrenalin;
    }

    public void setAdrenalin(int adrenalin) {
        if (adrenalin < 0)
            adrenalin = 0;
        if (adrenalin > 100)
            adrenalin = 100;
        this.adrenalin = adrenalin;
    }

    public int getConcentration() {
        return concentration;
    }

    public void setConcentration(int concentration) {
        if (concentration < 0)
            concentration = 0;
        if (concentration > 100)
            concentration = 100;
        this.concentration = concentration;
    }
}
