package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.sheet.modele.utility.Inventory;
import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;
import fr.anima.jdr.sheet.view.utility.ButtonEditor;
import fr.anima.jdr.sheet.view.utility.ButtonRenderer;
import fr.anima.jdr.sheet.view.utility.InventoryView;

import javax.swing.*;
import java.awt.*;

class InventoryPanel extends SubPanel {

    private Inventory inventory;
    private JPanel infoPanel;
    private JProgressBar sizeBar;
    private JTable inventoryTable;

    InventoryPanel(int width, int height, Color background, SheetPanel master) throws CharacterNotDefined {
        super(width, height, background, master);
    }

    InventoryPanel(SheetPanel master) throws CharacterNotDefined {
        super(master);
    }

    @Override
    void initComponents() throws CharacterNotDefined {
        this.inventory = this.getSheet().getCharacter().getInventory();

        // Inventory info
        this.sizeBar = new JProgressBar(0, this.inventory.getSize());
        this.sizeBar.setValue(this.inventory.getCurrentSize());
        this.sizeBar.setStringPainted(true);
        this.sizeBar.setString(this.inventory.getCurrentSize() + "/" + this.inventory.getSize());
        this.add(this.sizeBar, BorderLayout.NORTH);

        // Table
        InventoryView table = new InventoryView(this.inventory.getInventory());
        this.inventoryTable = new JTable(table);
        this.inventoryTable.getColumn("Modify").setCellRenderer(new ButtonRenderer());
        this.inventoryTable.getColumn("Modify").setCellEditor(new ButtonEditor(new JCheckBox()));
        JScrollPane scrollPane = new JScrollPane(this.inventoryTable);
        scrollPane.setPreferredSize(new Dimension(SubPanel.DEFAULT_WIDTH, 200));
        this.add(new JScrollPane(scrollPane), BorderLayout.CENTER);

        this.add(new JLabel("TEST"), BorderLayout.SOUTH);
    }

    void updateBars() {
        this.sizeBar.setMaximum(this.inventory.getSize());
        this.sizeBar.setValue(this.inventory.getCurrentSize());
        this.sizeBar.setStringPainted(true);
        this.sizeBar.setString(this.inventory.getCurrentSize() + "/" + this.inventory.getSize());
    }
}
