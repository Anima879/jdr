package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.equipment.controler.actions.character.ModifyAttributeAction;
import fr.anima.jdr.sheet.modele.character.Character;
import fr.anima.jdr.sheet.modele.utility.Buff;
import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;
import fr.anima.jdr.sheet.view.utility.BuffLabel;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

class AttributesPanel extends SubPanel {
    AttributesPanel(int width, int height, Color background, SheetPanel master) throws CharacterNotDefined {
        super(width, height, background, master);
    }

    AttributesPanel(SheetPanel master) throws CharacterNotDefined {
        super(master);
    }

    private List<JLabel> initialValues;

    @Override
    void initComponents() {
        this.initialValues = new ArrayList<>();
        this.setLayout(new BorderLayout());
        List<List<JLabel>> attributeList = new ArrayList<>();
        JPanel modifierCol = new JPanel();
        modifierCol.setLayout(new GridLayout(6, 2));
        for (Character.ATTRIBUTES att : Character.ATTRIBUTES.values()) {
            attributeList.add(generateAttributeRow(att));

            // Modifier button for attributes
            JButton plusButton = new JButton(new ModifyAttributeAction(this.getSheet(), 1, att));
            JButton minusButton = new JButton(new ModifyAttributeAction(this.getSheet(), -1, att));
            JButton testButton = new JButton();
            plusButton.setText("+");
            minusButton.setText("-");
            testButton.setText("E");
            modifierCol.add(minusButton);
            modifierCol.add(plusButton);
            modifierCol.add(testButton);
        }

        JPanel nameCol = new JPanel();
        JPanel attributeCol = new JPanel();

        nameCol.setLayout(new GridLayout(6, 1));
        attributeCol.setLayout(new GridLayout(6, 1));
        for (List<JLabel> row : attributeList) {
            nameCol.add(row.get(0));
            JPanel rowPanel = new JPanel();
            rowPanel.setLayout(new BoxLayout(rowPanel, BoxLayout.X_AXIS));
            for (int i = 1; i < row.size(); i++) {
                JLabel label = row.get(i);
                rowPanel.add(label);
                rowPanel.add(Box.createRigidArea(new Dimension(20, 0)));
            }
            attributeCol.add(rowPanel);
        }

        this.add(nameCol, BorderLayout.WEST);
        this.add(attributeCol, BorderLayout.CENTER);
        this.add(modifierCol, BorderLayout.EAST);
    }

    private @NotNull List<JLabel> generateAttributeRow(Character.@NotNull ATTRIBUTES att) {
        List<JLabel> row = new ArrayList<>();
        JLabel initialValue = new JLabel();
        JLabel attLabel = new JLabel();
        initialValue.setText(Integer.toString(this.getSheet().getCharacter().getAttribute(att)));
        this.initialValues.add(initialValue);
        switch (att) {
            case FORCE -> attLabel.setText("Force : ");
            case CHARISME -> attLabel.setText("Charisme : ");
            case ADRESSE -> attLabel.setText("Adresse : ");
            case COURAGE -> attLabel.setText("Courage : ");
            case INTELLIGENCE -> attLabel.setText("Intelligence : ");
            case INTUITION -> attLabel.setText("Intuition : ");
            default -> throw new IllegalStateException("Unexpected value: " + att);
        }

        List<JLabel> buffList = new ArrayList<>();
        if (!this.getSheet().getCharacter().getAttributeBuff(att).isEmpty()) {
            int finalValue = this.getSheet().getCharacter().getAttribute(att);
            for (Buff buff : this.getSheet().getCharacter().getAttributeBuff(att)) {
                finalValue += buff.getValue();
                buffList.add(new BuffLabel(buff));
            }
            buffList.add(new JLabel(Integer.toString(finalValue)));
        }

        row.add(attLabel);
        row.add(initialValue);
        row.addAll(buffList);

        return row;
    }

    public void updateValues() {
        this.removeAll();
        this.add(new JLabel("Test"));
        this.initComponents();
    }

    public List<JLabel> getInitialValues() {
        return initialValues;
    }
}
