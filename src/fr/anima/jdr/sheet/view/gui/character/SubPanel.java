package fr.anima.jdr.sheet.view.gui.character;

import fr.anima.jdr.sheet.view.gui.exceptions.CharacterNotDefined;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;

abstract class SubPanel extends JPanel {
    static final int DEFAULT_WIDTH = SheetPanel.DEFAULT_WIDTH;
    static final int DEFAULT_HEIGHT = 100;
    static final Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    private SheetPanel sheet;

    SubPanel(int width, int height, Color background, SheetPanel master) throws CharacterNotDefined {
        super();
        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(background);
        this.sheet = master;
        this.initComponents();
    }

    SubPanel(SheetPanel master) throws CharacterNotDefined {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_BACKGROUND_COLOR, master);
    }

    SheetPanel getSheet() {
        return sheet;
    }

    void setSheet(SheetPanel sheet) {
        this.sheet = sheet;
    }

    abstract void initComponents() throws CharacterNotDefined;
}
